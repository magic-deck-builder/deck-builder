#include "utils.hpp"

float deckBuilder::weight::compute_weight(const deckBuilder::Card &c1, const deckBuilder::Card &c2) {
	//TODO : Function to compute weight between two cards
	const auto color = color_weight(c1, c2);
	const auto capacity_ = capacity_weight(c1, c2);
	const auto supertype = supertype_weight(c1, c2);
	const auto type = type_weight(c1, c2);
	const auto subtype = subtype_weight(c1, c2);
	const auto tmp = (color + (0.8f * capacity_) + (0.15f * supertype) + (0.5f * type) + (0.15f * subtype));
	return tmp;
}