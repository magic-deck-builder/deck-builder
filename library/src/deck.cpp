#include "deck.hpp"

void deckBuilder::Deck::build_deck(const std::uint16_t seed,
								   const std::uint8_t size_deck,
								   const std::unordered_map<std::uint16_t, std::uint16_t> &card_id_to_index) {
	const deckBuilder::Graph &g = *graph;
	clear();
	//TODO : Function to build deck*
	cards.resize(size_deck);
	weight.resize(g.m_vertex_set.size());
	cards.at(0) = card_id_to_index.at(seed);

#pragma omp parallel for
	for (std::size_t j = 0; j < g.m_vertex_set.size(); ++j) {
		weight[j] = get(boost::edge_weight_t(), g, boost::edge(card_id_to_index.at(seed), j, g).first);
	}

	for (std::size_t k = 1; k < size_deck; ++k) {
		cards.at(k) = std::distance(weight.cbegin(), std::max_element(weight.cbegin(), weight.cend()));
		sync_weight(cards.at(k));
	}
}

void deckBuilder::Deck::sync_weight(const std::uint8_t index) {
	const Graph &g = *graph;
#pragma omp parallel for
	for (std::size_t j = 0; j < g.m_vertex_set.size(); ++j) {
		if (index == j) {
			weight[j] *= 0.2f;
			continue;
		}
		weight[j] = (weight[j] + (get(boost::edge_weight_t(), g, boost::edge(index, j, g).first) * 0.2f)) * 0.5f;
	}
}

void deckBuilder::Deck::clear() {
	cards.clear();
	weight.clear();
	size = 0;
}

std::ostream &operator<<(std::ostream &os, const deckBuilder::Deck &deck) {
	os << "MultiverseID,Count,Foil,Tag\n";
	for (const auto &item: deck.cards) {
		const auto tmp = (*deck.graph)[item].multiverse_id;
		os << '"' << tmp << R"(","1","","Deck")" << '\n';
	}
	return os;
}