#include "deck_builder.hpp"
#include <fstream>
#include <charconv>

deckBuilder::DeckBuilder::DeckBuilder(const std::filesystem::path &pathToDb,
									  const std::filesystem::path &pathToHelper,
									  const std::size_t amountOfCards) {
	const auto bdd = [&]() -> std::string {
		std::ifstream file{ pathToDb };
		std::stringstream stream;
		stream << file.rdbuf();
		return stream.str();
	}();
	const auto helper = [&]() -> std::string {
		std::ifstream file{ pathToHelper };
		std::stringstream stream;
		stream << file.rdbuf();
		return stream.str();
	}();
	cards = json_parser(bdd, helper, amountOfCards);
}

deckBuilder::Graph deckBuilder::DeckBuilder::json_parser(const std::string &bddJsonStr,
														 const std::string &nbJsonHelperStr,
														 const std::size_t amountOfCard) {
	const auto[nb_capacity, nb_legality, nb_supertype, nb_type, nb_subtype] = [&]() -> std::tuple<std::size_t, std::size_t, std::size_t, std::size_t, std::size_t> {
		simdjson::dom::parser helper_parser;
		const json helper = helper_parser.parse(simdjson::padded_string(nbJsonHelperStr));
		std::size_t a = 0, b = 0, c = 0, d = 0, e = 0;

		std::string_view chars = helper.at(0).at_key("count").get_string();
		std::from_chars(chars.cbegin(), chars.cend(), a);

		chars = helper.at(1).at_key("count").get_string();
		std::from_chars(chars.cbegin(), chars.cend(), b);

		chars = helper.at(2).at_key("count").get_string();
		std::from_chars(chars.cbegin(), chars.cend(), c);

		chars = helper.at(3).at_key("count").get_string();
		std::from_chars(chars.cbegin(), chars.cend(), d);

		chars = helper.at(4).at_key("count").get_string();
		std::from_chars(chars.cbegin(), chars.cend(), e);
		return { a, b, c, d, e };
	}();

	Graph local_cards(amountOfCard);
	{
		simdjson::dom::parser bdd_parser;
		const json bdd = bdd_parser.parse(simdjson::padded_string(bddJsonStr));
		auto bdd_iterator = bdd.begin();
		for (auto[vi, vi_end] = boost::vertices(local_cards); vi != vi_end; ++vi, ++bdd_iterator) {
			const auto &current_bdd_element = *bdd_iterator;
			const auto layout = str2int(current_bdd_element["cla_name"].get_c_str()); // this field is absent from the json but is needed, HELP.
			const std::size_t first_mana_cost = [&]() -> std::size_t {
				const std::string_view chars = current_bdd_element["car_convertedmanacost"].get_string();
				std::size_t result = 0;
				std::from_chars(chars.cbegin(), chars.cend(), result);
				return result;
			}();

			local_cards[*vi].capacity.resize(nb_capacity);
			local_cards[*vi].legality.resize(nb_legality);
			local_cards[*vi].supertype.resize(nb_supertype);
			local_cards[*vi].type.resize(nb_type);
			local_cards[*vi].subtype.resize(nb_subtype);

			switch (layout) {
				case str2int("split"):
				case str2int("flip"):
				case str2int("double-faced"):
				case str2int("aftermath"): {
					const auto &next_bdd_element = [&]() -> element {
						auto next_iter = bdd_iterator;
						++next_iter;
						return *next_iter;
					}();
					const std::size_t second_mana_cost = [&]() -> std::size_t {
						const std::string_view chars = next_bdd_element["car_convertedmanacost"].get_string();
						std::size_t result = 0;
						std::from_chars(chars.cbegin(), chars.cend(), result);
						return result;
					}();
					// "fusion" of local_cards.
					if (first_mana_cost >= second_mana_cost) {
						fill_card(local_cards[*vi], current_bdd_element, layout, first_mana_cost);
					} else {
						fill_card(local_cards[*vi], next_bdd_element, layout, second_mana_cost);
					}
					local_cards[*vi].colour_identity = convert_colour(current_bdd_element["car_coloridentity"].get<std::string_view>());
					++bdd_iterator;
					local_cards[*vi].colour_identity |= convert_colour((*bdd_iterator)["car_coloridentity"].get<std::string_view>());

					// If the card cannot be inserted and it does not exist, it fails. Otherwise, it is all good.
					if (!card_id_to_index.emplace(local_cards[*vi].card_id, *vi).second &&
						card_id_to_index.find(local_cards[*vi].card_id) == card_id_to_index.cend()) {
						throw std::runtime_error("Impossible to insert the card_id in the hash table.");
					}
				}
					break;

				default:
					fill_card(local_cards[*vi], current_bdd_element, layout, first_mana_cost);
					local_cards[*vi].colour_identity = convert_colour(current_bdd_element["car_coloridentity"].get<std::string_view>());

					// If the card cannot be inserted and it does not exist, it fails. Otherwise, it is all good.
					if (!card_id_to_index.emplace(local_cards[*vi].card_id, *vi).second &&
						card_id_to_index.find(local_cards[*vi].card_id) == card_id_to_index.cend()) {
						throw std::runtime_error("Impossible to insert the card_id in the hash table.");
					}
					break;
			}
		}
	}
	return local_cards;
}