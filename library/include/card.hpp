#ifndef MAGICDECKBUILDER_CARD_HPP
#define MAGICDECKBUILDER_CARD_HPP

#include <cstdint>
#include <boost/dynamic_bitset.hpp>

namespace deckBuilder {
	/**
	 * \brief This structure represents the vertices inside the graph and also a card from Magic The Gathering.
	 * \struct Card
	 */
	struct Card {
		//! Order of colours : White, Blue, Black, Red, Green, Colourless
		std::bitset<6> colour_identity;
		boost::dynamic_bitset<> capacity;
		boost::dynamic_bitset<> legality;
		boost::dynamic_bitset<> supertype;
		boost::dynamic_bitset<> type;
		boost::dynamic_bitset<> subtype;

		std::uint32_t multiverse_id{ 0 };
		std::uint16_t card_id{ 0 };
		std::uint8_t mana_cost{ 0 };
		std::uint8_t block{ 0 };
		std::uint8_t layout{ 0 };

		std::int8_t power{ 0 };
		std::int8_t toughness{ 0 };

		Card() = default;

		Card(const Card &card) = default;

		Card(Card &&card) noexcept {
			if (this != &card) {
				colour_identity = card.colour_identity;
				capacity = std::move(card.capacity);
				legality = std::move(card.legality);
				supertype = std::move(card.supertype);
				type = std::move(card.type);
				subtype = std::move(card.subtype);

				multiverse_id = card.multiverse_id;
				card_id = card.card_id;
				mana_cost = card.mana_cost;
				block = card.block;
				layout = card.layout;
				power = card.power;
				toughness = card.toughness;
			}
		}

		Card &operator=(const Card &card) = default;

		Card &operator=(Card &&card) noexcept {
			if (this != &card) {
				colour_identity = card.colour_identity;
				capacity = std::move(card.capacity);
				legality = std::move(card.legality);
				supertype = std::move(card.supertype);
				type = std::move(card.type);
				subtype = std::move(card.subtype);

				multiverse_id = card.multiverse_id;
				card_id = card.card_id;
				mana_cost = card.mana_cost;
				block = card.block;
				layout = card.layout;
				power = card.power;
				toughness = card.toughness;
			}
			return *this;
		}
	};
}


#endif //MAGICDECKBUILDER_CARD_HPP
