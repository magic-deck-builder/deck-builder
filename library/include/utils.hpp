#ifndef MAGICDECKBUILDER_UTILS_HPP
#define MAGICDECKBUILDER_UTILS_HPP

#include "card.hpp"

namespace deckBuilder::weight {
	[[gnu::always_inline]] inline float color_weight(const Card &c1, const Card &c2) {
		const auto result = c1.colour_identity & c2.colour_identity;
		return (c1.colour_identity.count() > c2.colour_identity.count() ?
				static_cast<float>(result.count()) / c1.colour_identity.count() :
				static_cast<float>(result.count()) / c2.colour_identity.count());
	}

	[[gnu::always_inline]] inline float capacity_weight(const Card &c1, const Card &c2) {
		const auto result = c1.capacity & c2.capacity;
		if (result.count() == 0)
			return 0.f;
		return (c1.capacity.count() > c2.capacity.count() ?
				static_cast<float>(result.count()) / c1.capacity.count() :
				static_cast<float>(result.count()) / c2.capacity.count());
	}

	[[gnu::always_inline]] inline float supertype_weight(const Card &c1, const Card &c2) {
		const auto result = c1.supertype & c2.supertype;
		if (result.count() == 0)
			return 0.f;
		return (c1.supertype.count() > c2.supertype.count() ?
				static_cast<float>(result.count()) / c1.supertype.count() :
				static_cast<float>(result.count()) / c2.supertype.count());
	}

	[[gnu::always_inline]] inline float type_weight(const Card &c1, const Card &c2) {
		const auto result = c1.type & c2.type;
		if (result.count() == 0)
			return 0.f;
		return (c1.type.count() > c2.type.count() ?
				static_cast<float>(result.count()) / c1.type.count() :
				static_cast<float>(result.count()) / c2.type.count());
	}

	[[gnu::always_inline]] inline float subtype_weight(const Card &c1, const Card &c2) {
		const auto result = c1.subtype & c2.subtype;
		if (result.count() == 0)
			return 0.f;
		return (c1.subtype.count() > c2.subtype.count() ?
				static_cast<float>(result.count()) / c1.subtype.count() :
				static_cast<float>(result.count()) / c2.subtype.count());
	}

	float compute_weight(const Card &c1, const Card &c2);
}

#endif //MAGICDECKBUILDER_UTILS_HPP
