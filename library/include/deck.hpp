#ifndef MAGICDECKBUILDER_DECK_HPP
#define MAGICDECKBUILDER_DECK_HPP

#include <vector>
#include <cstdint>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/property_map/property_map.hpp>
#include <ostream>
#include "card.hpp"

namespace deckBuilder {
	using Graph = boost::adjacency_matrix<boost::undirectedS, Card, boost::property<boost::edge_weight_t, float>>;

	/**
	 * \brief This is the structure which represents a deck from Magic The Gathering.
	 * \struct Deck
	 */
	struct Deck {
		std::vector<std::uint16_t> cards;
		std::vector<float> weight;
		std::uint8_t size{ 0 };
		const Graph *graph{ nullptr };

		Deck() = default;

		explicit Deck(const Graph &graph) : graph(&graph) {}

		/*Deck(const std::vector<uint16_t> &cards,
			 const std::vector<float> &weight,
			 const std::uint8_t &size,
			 const Graph &graph) :
				cards(cards),
				weight(weight),
				size(size),
				graph(&graph) {}*/

		Deck(const Deck &deck) = default;

		Deck(Deck &&deck) noexcept {
			cards = std::move(deck.cards);
			weight = std::move(deck.weight);
			size = deck.size;
			graph = deck.graph;
			deck.size = 0;
			deck.graph = nullptr;
		}

		Deck &operator=(const Deck &deck) = default;

		Deck &operator=(Deck &&deck) noexcept {
			if (this != &deck) {
				cards = std::move(deck.cards);
				weight = std::move(deck.weight);
				size = deck.size;
				graph = deck.graph;
			}
			return *this;
		}

		void build_deck(const std::uint16_t seed,
						const std::uint8_t size_deck,
						const std::unordered_map<std::uint16_t, std::uint16_t> &card_id_to_index);

		void sync_weight(const std::uint8_t index);

		/**
		 * \brief It clears all the vectors inside the deck.
		 */
		void clear();

		/**
		 * \brief It exports the deck in CSV format understandable for UrzaGatherer.
		 * \param os
		 * \param deck
		 * \return
		 */
		friend std::ostream &operator<<(std::ostream &os, const Deck &deck);
	};
}

#endif //MAGICDECKBUILDER_DECK_HPP
