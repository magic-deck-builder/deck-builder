#include <iostream>
#include <omp.h>
#include "parser.hpp"

//extern std::unordered_map<std::uint16_t, std::uint16_t> card_id_to_index; // To use it. Declared in parser.cpp

int main(int argc, char *argv[]) {
	double start = omp_get_wtime();

	auto cards = json_parser("notingit/mag_card.json", "notingit/mag_cardcapacity.json", 18155);
	double stop = omp_get_wtime();
	std::cout << stop - start << std::endl;
	auto deck = Deck(cards);

	size_t taille = cards.m_vertex_set.size();

	start = omp_get_wtime();

	for (size_t j = taille - 1; j > 0; --j) {
		for (size_t k = 0; k < taille; ++k) {
			if (k == j)
				continue;
			boost::add_edge(j, k, 0.f, cards);
		}
		--taille;
	}
	taille = cards.m_vertex_set.size();
	for (size_t j = taille - 1; j > 0; --j) {
		#pragma omp parallel for schedule(guided)
		for (size_t k = 0; k < taille; ++k) {
			if (k == j)
				continue;
			cards.get_edge(j, k).second.m_value = weight::compute_weight(cards[j], cards[k]);
		}
		--taille;
	}
	stop = omp_get_wtime();
//	using edge_iterator = boost::graph_traits<Graph>::edge_iterator;
//	boost::property_map<Graph, boost::edge_weight_t>::type EdgeWeightMap = get(boost::edge_weight_t(), cards);
//	std::pair<edge_iterator, edge_iterator> edgePair;
//	for (edgePair = edges(cards); edgePair.first != edgePair.second; ++edgePair.first) {
//		std::cout << *edgePair.first << " " << EdgeWeightMap[*edgePair.first] << std::endl;
//	}
//	for (int j = 0; j < cards.m_vertex_set.size(); ++j) {
//		std::cout << j << " et id card : " << cards[j].card_id << std::endl;
//	}
	std::cout << stop - start << std::endl;
	deck.build_deck(2054, 40);
//	boost::print_graph(cards);
//	std::cout << get(boost::edge_weight_t(), cards, boost::edge(0, 0, cards).first) << '\n';
	return EXIT_SUCCESS;
}