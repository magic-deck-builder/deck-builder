#define CATCH_CONFIG_ENABLE_BENCHMARKING

#include <catch2/catch.hpp>
#include <omp.h>
#include "utils.hpp"
#include "deck.hpp"
#include "deck_builder.hpp"

[[gnu::always_inline]] inline void initializeEdgSeq(deckBuilder::Graph &graph) {
	size_t taille = graph.m_vertex_set.size();
	for (size_t j = taille - 1; j > 0; --j) {
		for (size_t k = 0; k < taille; ++k) {
			boost::add_edge(j, k, std::numeric_limits<float>::min(), graph);
		}
		--taille;
	}
}

[[gnu::always_inline]] inline void computeGraphSeq(deckBuilder::Graph &graph) {
	size_t taille = graph.m_vertex_set.size();
	for (size_t j = taille - 1; j > 0; --j) {
		for (size_t k = 0; k < taille; ++k) {
			graph.get_edge(j, k).second.m_value =
					deckBuilder::weight::compute_weight(graph[j], graph[k]);
		}
		--taille;
	}
}

[[gnu::always_inline]] inline void computeGraphPar(deckBuilder::Graph &graph) {
	auto taille = graph.m_vertex_set.size();
	for (size_t j = taille - 1; j > 0; --j) {
#pragma omp parallel for schedule(runtime)
		for (size_t k = 0; k < taille; ++k) {
			graph.get_edge(j, k).second.m_value = deckBuilder::weight::compute_weight(graph[j], graph[k]);
		}
		--taille;
	}
}

TEST_CASE("Test: json parser", "[0][json_parser]") {
	CHECK_NOTHROW(deckBuilder::DeckBuilder(std::filesystem::path("db-cards-legacy/mag_card_1.json"), "db-cards-legacy/mag_cardcapacity.json", 16383));
}

/*TEST_CASE("Test : algorithm to initialize edges.", "[0][algo_graph]") {
	INFO("Parse JSON")
	auto cardsSequential = json_parser("db-cards-legacy/mag_card_1.json", "db-cards-legacy/mag_cardcapacity.json", 18155);
	auto cardsParallel = json_parser("db-cards-legacy/mag_card_1.json", "db-cards-legacy/mag_cardcapacity.json", 18155);

	BENCHMARK("[SEQUENTIAL] Initialize edges") {
												   initializeEdgSeq(cardsSequential);
											   };
	BENCHMARK("[SEQUENTIAL] Compute weights") {
												  computeGraphSeq(cardsSequential);
											  };
	BENCHMARK("[PARALLEL] Compute weights") {
												computeGraphPar(cardsParallel);
											};

	const auto mseq = get(boost::edge_weight, cardsSequential),
			mpar = get(boost::edge_weight, cardsParallel);
	INFO("Check weights value for sequential computed graph.")
	for (auto[edge, edge_end] = boost::edges(cardsSequential); edge != edge_end; ++edge) {
		REQUIRE(mseq[*edge] != std::numeric_limits<float>::min());
	}

	INFO("Check weights value for parallel computed graph.")
	for (auto[edge, edge_end] = boost::edges(cardsParallel); edge != edge_end; ++edge) {
		REQUIRE(mpar[*edge] != std::numeric_limits<float>::min());
	}
}*/
